const dias_semana = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
const meses = [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre'
];

function cargarImportes() {
    var inputs =
        '<div class="form-row"><div class="form-group col-md-6"><label for="letra">Importe de la cuota:</label><input readonly type="text" name="letra" class="form-control" id="letra"></div><div class="form-group col-md-6"><label for="total">Total pagado:</label><input readonly type="text" name="total" class="form-control" id="total"></div></div> <div class="container"><div class="row"><div class="col-md-3"></div><div class="col-md-6 text-center p-4"><button name="descargarExcel"  class="btn bg-success text-white mx-3 mb-3" id="descargarExcel" onclick="crearCSV()" disabled>Exportar a Excel</button><a class="btn bg-info text-white mx-3 mb-3" data-toggle="modal" data-target="#exampleModalCenter">Nuevo formulario</a></div></div><div class="col-md-3"></div><div class="form-group"><div id="resultado"></div></div></div>';

    return inputs;
}

function cargarCantidadCuotas() {
    var cantcuotas =
        '<div class="form-row"><div class="form-group col-md-6"><label for="cuotas">Cantidad de cuotas:</label><input readonly type="text" name="cuotas" class="form-control" id="cuotas" placeholder="Cuotas" required></div><div class="form-group col-md-6"><label for="tasadiaria">Tasa Diaria:</label><input readonly type="text" name="tasadiaria" class="form-control" id="tasadiaria" placeholder="Tasa Diaria" required></div></div><div><div class="container"><div class="row"><div class="col-md-3"></div><div class="col-md-6 text-center"><a href="#abajo" name="enviar"  class="btn btn-primary btn-block" id="enviar" onclick="calcular()">Calcular</a></div></div><div class="col-md-3"></div></div></div>';
    return cantcuotas;
}

function cargardiaDepago(valor) {
    if (valor == 1) {
        document.querySelector('#input_dia_pago').innerHTML = cargarSelectSemanas();
    } else if (valor == 2) {
        document.querySelector('#input_dia_pago').innerHTML = cargarSelectQuincenal();
    } else if (valor == 3) {
        document.querySelector('#input_dia_pago').innerHTML = cargarSelectDiasMes();
    } else if (valor == 4) {
        document.querySelector('#input_dia_pago').innerHTML = cargarSelectDiasMes();
    } else if (valor == 5) {
        document.querySelector('#input_dia_pago').innerHTML = cargarSelectDiasMes();
    }
}

function cargarVector(valor) {
    if (valor == 2) {
        document.querySelector('#vectores').innerHTML = cargarVectorQuincenal();
    } else {
        document.querySelector('#vectores').innerHTML = cargarVectorMensual();
    }
    document.getElementById('guardar').disabled = false;
}

function cargarSelectSemanas() {
    var select_dia_pago =
        "<label for='diapago'>Elija un dia de pago</label><select name='diapago' id='diapago' class='custom-select' required><option value='0'>Elige un día de pago</option><option value='1'>Lunes</option><option value='2'>Martes</option><option value='3'>Miércoles</option><option value='4'>Jueves</option><option value='5'>Viernes</option><option value='6'>Sábado</option><option value='7'>Domingo</option></select> ";
    return select_dia_pago;
}

function cargarSelectDiasMes() {
    var select_dia_mes =
        "<label for='diapago'>Elija un dia del mes para pagar</label><input class='form-control' type='number' name='diapago' id='diapago' placeholder='Escribe un día del mes' value='1' min='1' max='31' step='1'required> ";
    return select_dia_mes;
}

function cargarSelectQuincenal() {
    var select_dia_quincenal =
        "<label for='diapago1'>Cuota 1 [13-17]</label><input class='form-control' type='number' name='diapago1' id='diapago1' value='14' min='13' max='17' step='1'required> <label for='diapago2'>Cuota 2 [28-3]</label><input class='form-control' type='number' name='diapago2' id='diapago2' value='30' step='1' min='1' max='31' required>";
    return select_dia_quincenal;
}

function cargarVectorQuincenal() {
    var vector_quincenal =
        "<label for='vectorQuincenal1'>Primera fecha</label><input type='number' class='form-control' name='vectorQuincenal1' id='vectorQuincenal1' value='1' min='0' max='10' step='0.25'><label for='vectorQuincenal2'>Segunda fecha</label><input type='number' class='form-control' name='vectorQuincenal2' id='vectorQuincenal2' value='1' min='0' max='10' step='0.25'>";
    return vector_quincenal;
}

function cargarVectorMensual() {
    var vector_mensual =
        "<label for='vectorMensual'>Vector del mes</label><input type='number' class='form-control' name='vectorMensual' id='vectorMensual' value='1' min='0' max='10' step='0.25'>";
    return vector_mensual;
}
var mes_especial = [];
var vector_mensual = [];

function guardar_mes_especial() {
    if (obtener_forma_pago() != 'Quincenal') {
        var inputmes = document.getElementById('mesEspecial').value;
        var ms = meses[inputmes];
        var vs = document.getElementById('vectorMensual').value;
        if (inputmes != 12 && vs != 1) {
            mes_especial.push(ms);
            vector_mensual.push(vs);
        }
        document.querySelector('#mesespecial').innerHTML = generarTablaMesesEspeciales(mes_especial, vector_mensual);
        document.ready = document.getElementById('mesEspecial').value = '12';
        document.ready = document.getElementById('vectorMensual').value = '1';
    } else {
        var inputmes = document.getElementById('mesEspecial').value;
        var diapago2 = obtener_diapago_quincenal2();

        var ms = meses[inputmes];
        var vs1 = document.getElementById('vectorQuincenal1').value;
        var vs2 = document.getElementById('vectorQuincenal2').value;
        if (inputmes != 12) {
            mes_especial.push(ms);
            vector_mensual.push(vs1);
        }
        if (inputmes != 12) {
            mes_especial.push(ms);
            vector_mensual.push(vs2);
        }
        document.querySelector('#mesespecial').innerHTML = generarTablaMesesEspeciales(mes_especial, vector_mensual);
        document.ready = document.getElementById('mesEspecial').value = '12';
        document.ready = document.getElementById('vectorQuincenal1').value = '1';
        document.ready = document.getElementById('vectorQuincenal2').value = '1';
    }
}

function generarTablaMesesEspeciales(mes_especial, vector_mensual) {
    var tabla1 =
        "<table id='tablita1' class='table table-hover table-bordered'> <thead class='thead-light'> <tr><th scope='col'>Mes</th><th scope='col' colspan='5'>Vector mensual</th></tr></thead>";
    tabla1 += '<tbody>';
    for (var i = 0; i < mes_especial.length; i++) {
        tabla1 +=
            '<tr><td>' +
            mes_especial[i] +
            '</td><td>' +
            vector_mensual[i] +
            '</td><td><button name="eliminar" id="eliminar" class="btn" onclick="remove(this);">Eliminar <i class="fas fa-trash-alt"></i></button></td></tr>';
    }
    tabla1 += '</tbody>';
    tabla1 += '</table>';
    return tabla1;
}

function remove(el) {
    var index = $(el).closest('tr').index();
    mes_especial.splice(index, 1);
    vector_mensual.splice(index, 1);
    $(el).closest('tr').remove();
}

function obtener_forma_pago() {
    var fpg = document.getElementById('formapago');
    var formapago = fpg.options[fpg.selectedIndex].text;
    return formapago;
}

function obtener_dia_pago() {
    var formapago = obtener_forma_pago();
    if (formapago == 'Semanal') {
        var dp = document.getElementById('diapago');
        var diapago = dp.options[dp.selectedIndex].text;
    } else {
        var diapago = document.getElementById('diapago').value;
    }
    return diapago;
}

function obtener_diapago_quincenal1() {
    var diapago1 = document.getElementById('diapago1').value;
    return diapago1;
}

function obtener_diapago_quincenal2() {
    var diapago2 = document.getElementById('diapago2').value;
    if (diapago2 == 1) {
        diapago2 = 32;
    } else if (diapago2 == 2) {
        diapago2 = 33;
    } else if (diapago2 == 3) {
        diapago2 = 34;
    }
    return diapago2;
}

function calcular_fecha_desemboloso() {
    var fd = document.getElementById('fechaDesembolso').value;
    var diad = fd.slice(8, 10);
    var mesd = fd.slice(5, 7) - 1;
    var aniod = fd.slice(0, 4);
    var dia = parseInt(diad);
    var mes = parseInt(mesd);
    var anio = parseInt(aniod);
    var fecha_desembolso = new Date(anio, mes, dia);
    return fecha_desembolso;
}

function calcular_fecha_primera_cuota() {
    var fp1 = document.getElementById('fechaPrimeraCuota').value;
    var fp = String(fp1);
    var formapago = obtener_forma_pago();
    if (formapago == 'Quincenal') {
        var diapago1 = obtener_diapago_quincenal1();
        var diapago2 = obtener_diapago_quincenal2();
    } else {
        var diapago = obtener_dia_pago();
    }

    var fecha_desembolso = calcular_fecha_desemboloso();
    var mes = parseInt(fecha_desembolso.getMonth());
    var dia = parseInt(fecha_desembolso.getDate());
    var anio = parseInt(fecha_desembolso.getFullYear());
    if (fp === '') {
        switch (formapago) {
            case 'Semanal':
                if (diapago == dias_semana[fecha_desembolso.getDay()]) {
                    dia = dia + 7;
                    var fecha_primera_cuota = new Date(anio, mes, dia);
                } else {
                    while (diapago != dias_semana[fecha_desembolso.getDay()]) {
                        dia = dia + 1;
                        fecha_desembolso = new Date(anio, mes, dia);
                    }
                    var fecha_primera_cuota = new Date(anio, mes, dia);
                }
                fecha_desembolso = calcular_fecha_desemboloso();
                var diferencia = (fecha_primera_cuota.getTime() - fecha_desembolso.getTime()) / (1000 * 60 * 60 * 24);
                if (diferencia <= 3) {
                    dia = dia + 7;
                    var fecha_primera_cuota = new Date(anio, mes, dia);
                }
                break;
            case 'Quincenal':
                if (diapago2 > 28 && meses[fecha_desembolso.getMonth()] == 'Febrero') {
                    var bisiesto = calcularBisiesto(fecha_desembolso.getFullYear());

                    if (bisiesto == true) {
                        diapago2 = 29;
                    } else {
                        diapago2 = 28;
                    }
                }

                if (dia <= 7) {
                    dia = diapago1;
                    var fecha_primera_cuota = new Date(anio, mes, dia);
                } else if (dia > 8 && dia <= 21) {
                    if (
                        meses[fecha_desembolso.getMonth()] == 'Abril' ||
                        meses[fecha_desembolso.getMonth()] == 'Junio' ||
                        meses[fecha_desembolso.getMonth()] == 'Septiembre' ||
                        meses[fecha_desembolso.getMonth()] == 'Noviembre'
                    ) {
                        diapago2 = diapago2 - 1;
                    }
                    dia = diapago2;
                    var fecha_primera_cuota = new Date(anio, mes, dia);
                } else {
                    mes = mes + 1;
                    dia = diapago1;
                    var fecha_primera_cuota = new Date(anio, mes, dia);
                }
                break;
            case 'Mensual':
                if (diapago > 28 && meses[fecha_desembolso.getMonth()] == 'Febrero') {
                    var bisiesto = calcularBisiesto(fecha_desembolso.getFullYear());

                    if (bisiesto == true) {
                        diapago = 29;
                    } else {
                        diapago = 28;
                    }
                } else if (
                    (meses[fecha_desembolso.getMonth()] == 'Abril' ||
                        meses[fecha_desembolso.getMonth()] == 'Junio' ||
                        meses[fecha_desembolso.getMonth()] == 'Septiembre' ||
                        meses[fecha_desembolso.getMonth()] == 'Noviembre') &&
                    diapago == 31
                ) {
                    diapago = 30;
                }
                if (dia >= diapago) {
                    dia = diapago;
                    mes = mes + 1;
                    var fecha_primera_cuota = new Date(anio, mes, dia);
                } else {
                    dia = diapago;
                    var fecha_primera_cuota = new Date(anio, mes, dia);
                }

                var diferencia = (fecha_primera_cuota.getTime() - fecha_desembolso.getTime()) / (1000 * 60 * 60 * 24);
                if (diferencia <= 15) {
                    mes = mes + 1;
                    if (diapago == 31 && (mes == 3 || mes == 5 || mes == 8 || mes == 10)) {
                        dia = 30;
                    } else if (diapago <= 31 && diapago >= 28 && mes == 1) {
                        var bisiesto = calcularBisiesto(fecha_primera_cuota.getFullYear());

                        if (bisiesto == true) {
                            dia = 29;
                        } else {
                            dia = 28;
                        }
                    }

                    var fecha_primera_cuota = new Date(anio, mes, dia);
                }
                break;
            case 'Bimestral':
                if (diapago > 28 && meses[fecha_desembolso.getMonth()] == 'Febrero') {
                    var bisiesto = calcularBisiesto(fecha_desembolso.getFullYear());

                    if (bisiesto == true) {
                        diapago = 29;
                    } else {
                        diapago = 28;
                    }
                } else if (
                    (meses[fecha_desembolso.getMonth()] == 'Abril' ||
                        meses[fecha_desembolso.getMonth()] == 'Junio' ||
                        meses[fecha_desembolso.getMonth()] == 'Septiembre' ||
                        meses[fecha_desembolso.getMonth()] == 'Noviembre') &&
                    diapago == 31
                ) {
                    diapago = 30;
                }
                if (dia >= diapago) {
                    dia = diapago;
                    mes = mes + 1;
                    var fecha_primera_cuota = new Date(anio, mes, dia);
                } else {
                    dia = diapago;
                    var fecha_primera_cuota = new Date(anio, mes, dia);
                }

                var diferencia = (fecha_primera_cuota.getTime() - fecha_desembolso.getTime()) / (1000 * 60 * 60 * 24);
                if (diferencia <= 30) {
                    mes = mes + 1;
                    if (diapago == 31 && (mes == 3 || mes == 5 || mes == 8 || mes == 10)) {
                        dia = 30;
                    } else if (diapago <= 31 && diapago >= 28 && mes == 1) {
                        var bisiesto = calcularBisiesto(fecha_desembolso.getFullYear());

                        if (bisiesto == true) {
                            dia = 29;
                        } else {
                            dia = 28;
                        }
                    }
                    var fecha_primera_cuota = new Date(anio, mes, dia);
                }
                break;
            case 'Trimestral':
                if (diapago > 28 && meses[fecha_desembolso.getMonth()] == 'Febrero') {
                    var bisiesto = calcularBisiesto(fecha_desembolso.getFullYear());

                    if (bisiesto == true) {
                        diapago = 29;
                    } else {
                        diapago = 28;
                    }
                } else if (
                    (meses[fecha_desembolso.getMonth()] == 'Abril' ||
                        meses[fecha_desembolso.getMonth()] == 'Junio' ||
                        meses[fecha_desembolso.getMonth()] == 'Septiembre' ||
                        meses[fecha_desembolso.getMonth()] == 'Noviembre') &&
                    diapago == 31
                ) {
                    diapago = 30;
                }
                if (dia >= diapago) {
                    dia = diapago;
                    mes = mes + 1;
                    var fecha_primera_cuota = new Date(anio, mes, dia);
                } else {
                    dia = diapago;
                    var fecha_primera_cuota = new Date(anio, mes, dia);
                }

                var diferencia = (fecha_primera_cuota.getTime() - fecha_desembolso.getTime()) / (1000 * 60 * 60 * 24);
                if (diferencia <= 45) {
                    mes = mes + 1;
                    if (diapago == 31 && (mes == 3 || mes == 5 || mes == 8 || mes == 10)) {
                        dia = 30;
                    } else if (diapago <= 31 && diapago >= 28 && mes == 1) {
                        var bisiesto = calcularBisiesto(fecha_desembolso.getFullYear());

                        if (bisiesto == true) {
                            dia = 29;
                        } else {
                            dia = 28;
                        }
                    }
                    var fecha_primera_cuota = new Date(anio, mes, dia);
                }
                break;
        }
    } else {
        var fp1 = document.getElementById('fechaPrimeraCuota').value;
        var fp = String(fp1);
        var diap = fp.slice(8, 10);
        var mesp = fp.slice(5, 7) - 1;
        var aniop = fp.slice(0, 4);
        var dia = parseInt(diap);
        var mes = parseInt(mesp);
        var anio = parseInt(aniop);
        var fecha_primera_cuota = new Date(anio, mes, dia);
    }
    return fecha_primera_cuota;
}



function calcularTN() {
    var tasa = document.getElementById('tasaanual').value;
    var tasaanual = parseFloat(tasa);
    var tasaanual = tasaanual;
    return tasaanual / 360;
}

function calcularTEA() {
    var tasa = document.getElementById('tasaanual').value;
    var tasaanual = parseFloat(tasa);
    var tasaanual = tasaanual;
    return Math.pow(1 + tasaanual, 1 / 365) - 1;
}

function calcularBisiesto(anio) {
    if (anio % 4 == 0 && (anio % 100 != 0 || anio % 400 == 0)) {
        return true;
    } else {
        return false;
    }
}


$(document).ready(function() {
    $('#calcularcuotas').click(function() {
        if ($('#prestamo').val() == "" || $('#plazo').val() == "" || $('#formapago').val() == "" || $('#diapago').val() == "" || $('#tasaanual').val() == "" || $('#fechaDesembolso').val() == "") {
            document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>¡Por favor!</strong> Agrega todos los campos obligatorios<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }
        var plazo1 = document.getElementById('plazo').value;
        var plazo = parseInt(plazo1);
        var unidad = $('input[type=radio][name=inlineRadioOptions]:checked').val();
        var formapago = obtener_forma_pago();
        var fecha_primera_cuota = calcular_fecha_primera_cuota();
        var mes = parseInt(fecha_primera_cuota.getMonth());
        var dia = parseInt(fecha_primera_cuota.getDate());
        var anio = parseInt(fecha_primera_cuota.getFullYear());
        if (unidad == 1) {
            mes = mes + plazo;
            var fecha_ultimo_dia = new Date(anio, mes, dia);
        } else {
            anio = anio + plazo;
            var fecha_ultimo_dia = new Date(anio, mes, dia);
        }

        var cuotas;
        switch (formapago) {
            case 'Semanal':
                var dias_totales;

                dias_totales = (fecha_ultimo_dia.getTime() - fecha_primera_cuota.getTime()) / (1000 * 60 * 60 * 24);
                cuotas = dias_totales / 7;
                break;

            case 'Quincenal':
                if (unidad == 1) {
                    cuotas = plazo * 2;
                } else {
                    cuotas = plazo * 24;
                }
                break;

            case 'Mensual':
                if (unidad == 1) {
                    cuotas = plazo;
                } else {
                    cuotas = plazo * 12;
                }
                break;

            case 'Bimestral':
                if (unidad == 1) {
                    cuotas = plazo / 2;
                } else {
                    cuotas = plazo * 6;
                }
                break;
            case 'Trimestral':
                if (unidad == 1) {
                    cuotas = plazo / 3;
                } else {
                    cuotas = plazo * 4;
                }
                break;
        }
        cuotas = Math.ceil(cuotas);
        document.querySelector('#cantidadcuotas').innerHTML = cargarCantidadCuotas();
        document.ready = document.getElementById('cuotas').value = cuotas;
        var tipo_tasa = $('input[type=radio][name=inlineRadioOptions1]:checked').val();
        if (tipo_tasa == 1) {
            document.ready = document.getElementById('tasadiaria').value = calcularTN().toFixed(5);
        } else {
            document.ready = document.getElementById('tasadiaria').value = calcularTEA().toFixed(5);
        }

    })
})

function calcular() {

    var cuotas = document.getElementById('cuotas').value;
    var formapago = obtener_forma_pago();
    if (formapago == 'Quincenal') {
        var diapago1 = obtener_diapago_quincenal1();
        var diapago2 = obtener_diapago_quincenal2();
    } else {
        var diapago = obtener_dia_pago();
    }
    var fecha_desembolso = calcular_fecha_desemboloso();
    var fecha_primera_cuota = calcular_fecha_primera_cuota();
    var dia = parseInt(fecha_primera_cuota.getDate());
    var mes = parseInt(fecha_primera_cuota.getMonth());
    var anio = parseInt(fecha_primera_cuota.getFullYear());

    var f_d = fecha_desembolso;
    var fechac = new Date(anio, mes, dia);

    var fechas_cuotas = [];
    fechas_cuotas.push(fecha_primera_cuota);
    var i = 0;

    switch (formapago) {
        case 'Semanal':
            if (diapago == dias_semana[fecha_primera_cuota.getDay()]) {
                while (i < cuotas) {
                    dia = dia + 7;
                    fechac = new Date(anio, mes, dia);
                    fechas_cuotas.push(fechac);
                    i++;
                }
            } else {
                while (diapago != dias_semana[fecha_primera_cuota.getDay()]) {
                    dia = dia + 1;
                    fecha_primera_cuota = new Date(anio, mes, dia);
                }

                while (i < cuotas) {
                    fechac = new Date(anio, mes, dia);
                    dia = dia + 7;
                    fechas_cuotas.push(fechac);
                    i++;
                }
            }
            break;

        case 'Quincenal':
            while (i < cuotas) {
                diapago2 = obtener_diapago_quincenal2();
                if (dia <= 17 && dia > 4) {
                    if (diapago2 < 32 && diapago2 > 28 && meses[fechac.getMonth()] == 'Febrero') {
                        var bisiesto = calcularBisiesto(fechac.getFullYear());
                        if (bisiesto == true) {
                            diapago2 = 29;
                        } else {
                            diapago2 = 28;
                        }
                    } else if (
                        (meses[fechac.getMonth()] == 'Abril' ||
                            meses[fechac.getMonth()] == 'Junio' ||
                            meses[fechac.getMonth()] == 'Septiembre' ||
                            meses[fechac.getMonth()] == 'Noviembre') &&
                        diapago2 >= 32
                    ) {
                        diapago2 = diapago2 - 1;
                    } else if (diapago2 >= 32 && meses[fechac.getMonth()] == 'Febrero') {
                        var bisiesto = calcularBisiesto(fechac.getFullYear());
                        if (bisiesto == true) {
                            diapago2 = diapago2 - 2;
                        } else {
                            diapago2 = diapago2 - 3;
                        }
                    } else if (
                        diapago2 == 31 &&
                        (meses[fechac.getMonth()] == 'Abril' ||
                            meses[fechac.getMonth()] == 'Junio' ||
                            meses[fechac.getMonth()] == 'Septiembre' ||
                            meses[fechac.getMonth()] == 'Noviembre')
                    ) {
                        diapago2 = 30;
                    }
                    dia = diapago2;
                    fechac = new Date(anio, mes, dia);
                    fechas_cuotas.push(fechac);
                } else if (dia <= 3) {
                    dia = diapago1;
                    fechac = new Date(anio, mes, dia);
                    fechas_cuotas.push(fechac);
                } else {
                    dia = diapago1;
                    mes = mes + 1;
                    fechac = new Date(anio, mes, dia);
                    fechas_cuotas.push(fechac);
                }
                i++;
            }
            break;

        case 'Mensual':
            var diac;
            diac = diapago;
            while (i < cuotas) {
                if (diac > 28 && meses[fechac.getMonth()] == 'Enero') {
                    var bisiesto = calcularBisiesto(fechac.getFullYear());

                    if (bisiesto == true) {
                        diac = 29;
                    } else {
                        diac = 28;
                    }
                } else if (
                    (meses[fechac.getMonth()] == 'Marzo' ||
                        meses[fechac.getMonth()] == 'Mayo' ||
                        meses[fechac.getMonth()] == 'Agosto' ||
                        meses[fechac.getMonth()] == 'Octubre') &&
                    diapago == 31
                ) {
                    diac = 30;
                } else {
                    diac = diapago;
                }

                mes = mes + 1;
                fechac = new Date(anio, mes, diac);
                fechas_cuotas.push(fechac);

                i++;
            }
            break;

        case 'Bimestral':
            var diac;
            diac = diapago;
            while (i < cuotas) {
                if (diac > 28 && meses[fechac.getMonth()] == 'Diciembre') {
                    var bisiesto = calcularBisiesto(fechac.getFullYear() + 1);

                    if (bisiesto == true) {
                        diac = 29;
                    } else {
                        diac = 28;
                    }
                } else if (
                    (meses[fechac.getMonth()] == 'Febrero' ||
                        meses[fechac.getMonth()] == 'Abril' ||
                        meses[fechac.getMonth()] == 'Julio' ||
                        meses[fechac.getMonth()] == 'Septiembre') &&
                    diapago == 31
                ) {
                    diac = 30;
                } else {
                    diac = diapago;
                }
                mes = mes + 2;
                fechac = new Date(anio, mes, diac);
                fechas_cuotas.push(fechac);

                i++;
            }
            break;

        case 'Trimestral':
            var diac;
            diac = diapago;
            while (i < cuotas) {
                if (diac > 28 && meses[fechac.getMonth()] == 'Noviembre') {
                    var bisiesto = calcularBisiesto(fechac.getFullYear() + 1);

                    if (bisiesto == true) {
                        diac = 29;
                    } else {
                        diac = 28;
                    }
                } else if (
                    (meses[fechac.getMonth()] == 'Enero' ||
                        meses[fechac.getMonth()] == 'Marzo' ||
                        meses[fechac.getMonth()] == 'Junio' ||
                        meses[fechac.getMonth()] == 'Agosto') &&
                    diapago == 31
                ) {
                    diac = 30;
                } else {
                    diac = diapago;
                }
                mes = mes + 3;
                fechac = new Date(anio, mes, diac);
                fechas_cuotas.push(fechac);

                i++;
            }
            break;
    }

    var vector_por_cuota = [];
    var fechas_de_cuota = [];
    var fechasCuotas = [];
    var primera_fila = calcular_fecha_desemboloso();
    var dateCuota = primera_fila.getDay();
    var nombrediaCuota = dias_semana[dateCuota];
    var diaCuota = primera_fila.getDate();
    var mesCuota = primera_fila.getMonth();
    var nombremesCuota = meses[mesCuota];
    var anioCuota = primera_fila.getFullYear();
    primera_fila = nombrediaCuota + ' ' + diaCuota + ' de ' + nombremesCuota + ' del ' + anioCuota;
    mesC = mesCuota + 1;
    primera_fila1 = diaCuota + '-' + mesC + '-' + anioCuota;
    for (var i = 0; i < fechas_cuotas.length; i++) {
        var dateCuota = fechas_cuotas[i].getDay();
        var nombrediaCuota = dias_semana[dateCuota];
        var diaCuota = fechas_cuotas[i].getDate();
        var mesCuota = fechas_cuotas[i].getMonth();
        var nombremesCuota = meses[mesCuota];
        var index = mes_especial.indexOf(nombremesCuota);
        if (obtener_forma_pago() != 'Quincenal') {
            if (index == -1) {
                vector_por_cuota[i] = 1;
            } else {
                vector_por_cuota[i] = vector_mensual[index];
            }
        } else {
            if (diaCuota < 4) {
                if (index == -1) {
                    vector_por_cuota[i] = 1;
                } else {
                    vector_por_cuota[i] = vector_mensual[index];
                }
            } else if (diaCuota >= 4 && diaCuota < 18) {
                if (obtener_diapago_quincenal2() < 31) {
                    if (index == -1) {
                        vector_por_cuota[i] = 1;
                    } else {
                        vector_por_cuota[i] = vector_mensual[index];
                    }
                } else {
                    if (index == -1) {
                        vector_por_cuota[i] = 1;
                    } else {
                        index = index + 1;
                        vector_por_cuota[i] = vector_mensual[index];
                    }
                }
            } else {
                if (index == -1) {
                    vector_por_cuota[i] = 1;
                } else {
                    index = index + 1;
                    vector_por_cuota[i] = vector_mensual[index];
                }
            }
        }
        var anioCuota = fechas_cuotas[i].getFullYear();
        mesC = mesCuota + 1;
        fechas_de_cuota[i] = nombrediaCuota + ' ' + diaCuota + ' de ' + nombremesCuota + ' del ' + anioCuota;
        fechasCuotas[i] = diaCuota + '-' + mesC + '-' + anioCuota;
    }

    var dias_diferencia = [];
    var dias_diferencia_acumulado = [];
    var x = 1;
    dias_diferencia[0] = (fechas_cuotas[0].getTime() - f_d.getTime()) / (1000 * 60 * 60 * 24);
    dias_diferencia_acumulado[0] = dias_diferencia[0];
    while (x < cuotas) {
        dias_diferencia[x] = (fechas_cuotas[x].getTime() - fechas_cuotas[x - 1].getTime()) / (1000 * 60 * 60 * 24);
        dias_diferencia_acumulado[x] = dias_diferencia_acumulado[x - 1] + dias_diferencia[x];
        x++;
    }

    var tasadiaria = document.getElementById('tasadiaria').value;

    var prestamo = document.getElementById('prestamo').value;

    var denominador = 0;

    for (var l = 0; l < cuotas; l++) {
        var termino = vector_por_cuota[l] * Math.pow(1 + tasadiaria / 100.0, dias_diferencia_acumulado[l] * -1);
        denominador = denominador + termino;
    }
    var letra = prestamo / denominador;

    var letra_por_cuota = [];
    for (let index = 0; index < cuotas; index++) {
        letra_por_cuota[index] = letra * vector_por_cuota[index];
    }

    var principal_al_inicio = [];
    var interes_tabla = [];
    var capital = [];
    var principal_al_final = [];
    principal_al_inicio[0] = prestamo;
    for (let index = 0; index < cuotas; index++) {
        if (vector_por_cuota[index] == 0) {
            principal_al_final[index] =
                principal_al_inicio[index] * Math.pow(1 + tasadiaria / 100, dias_diferencia[index]);
            principal_al_final[index] = principal_al_final[index];
            interes_tabla[index] = 0;
            capital[index] = 0;
            principal_al_inicio[index + 1] = principal_al_final[index];
        } else {
            interes_tabla[index] =
                principal_al_inicio[index] * Math.pow(1 + tasadiaria / 100, dias_diferencia[index]) -
                principal_al_inicio[index];
            interes_tabla[index] = interes_tabla[index];
            capital[index] = letra_por_cuota[index] - interes_tabla[index];
            capital[index] = capital[index];
            principal_al_final[index] = principal_al_inicio[index] - capital[index];
            principal_al_final[index] = principal_al_final[index];
            principal_al_inicio[index + 1] = principal_al_final[index];
        }
    }

    var total_pagado = 0.0;
    for (let index = 0; index < cuotas; index++) {
        total_pagado = total_pagado + parseFloat(letra_por_cuota[index]);
    }
    document.querySelector('#importes').innerHTML = cargarImportes();
    document.ready = document.getElementById('letra').value = letra.toFixed(5);
    document.ready = document.getElementById('total').value = total_pagado.toFixed(5);
    document.querySelector('#resultado').innerHTML = generarTabla(
        primera_fila,
        primera_fila1,
        cuotas,
        fechas_de_cuota,
        dias_diferencia,
        dias_diferencia_acumulado,
        principal_al_inicio,
        interes_tabla,
        capital,
        vector_por_cuota,
        letra_por_cuota,
        principal_al_final,
        fechasCuotas
    );

    document.getElementById('descargarExcel').disabled = false;
}

function generarTabla(
    primera_fila,
    primera_fila1,
    cuotas,
    fechas_de_cuota,
    dias_diferencia,
    dias_diferencia_acumulado,
    principal_al_inicio,
    interes_tabla,
    capital,
    vector_por_cuota,
    letra_por_cuota,
    principal_al_final,
    fechasCuotas
) {
    tn = calcularTN().toFixed(5);
    tea = calcularTEA().toFixed(5);
    var tabla =
        "<div id='resultado1' class='table-responsive'><table id='tablita' class='table table-hover table-bordered' style:'margin: 0 auto;'><thead class='thead-light align-middle'><tr><th scope='col' colspan='5'>Tasa diara nominal</th><th scope='col' colspan='6'>Tasa diaria efectiva</th></tr></thead><tbody><tr><td colspan='5'>" +
        tn +
        '</td><td colspan="6">' +
        tea +
        "</td></tr></tbody> <thead class='thead-light align-middle'> <tr><th scope='col'>Cuota</th><th scope='col'>Fecha de Cuota</th><th scope='col'>Días</th><th scope='col'>Dias de diferencia</th><th scope='col'>Dias de diferencia acumulado</th><th scope='col'>Principal al inicio</th><th scope='col'>Interés</th><th scope='col'>Capital</th><th scope='col'>Vector Mensual</th><th scope='col'>Total</th><th scope='col'>Principal al final</th></tr></thead>";
    tabla += '<tbody>';
    tabla +=
        '<tr><td>0</td><td>' +
        primera_fila1 +
        '</td><td>' +
        primera_fila +
        '</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
    var a = 1;
    for (var i = 0; i < cuotas; i++) {
        letra_por_cuota[i] = letra_por_cuota[i].toFixed(2);
        principal_al_final[i] = principal_al_final[i].toFixed(2);
        principal_al_inicio[i + 1] = principal_al_inicio[i + 1].toFixed(2);
        interes_tabla[i] = interes_tabla[i].toFixed(2);
        capital[i] = capital[i].toFixed(2);
        tabla +=
            '<tr><td>' +
            a +
            '</td><td>' +
            fechasCuotas[i] +
            '</td><td>' +
            fechas_de_cuota[i] +
            '</td><td>' +
            dias_diferencia[i] +
            '</td><td>' +
            dias_diferencia_acumulado[i] +
            '</td><td>' +
            principal_al_inicio[i] +
            '</td><td>' +
            interes_tabla[i] +
            '</td><td>' +
            capital[i] +
            '</td><td>' +
            vector_por_cuota[i] +
            '</td><td>' +
            letra_por_cuota[i] +
            '</td><td>' +
            principal_al_final[i] +
            '</td></tr>';
        a++;
    }
    tabla += '</tbody>';
    tabla += '</table></div>';
    return tabla;
}

var tableToExcel = (function() {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) },
        format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
    return function(table, name) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
        window.location.href = uri + base64(format(template, ctx))
    }
})()

function crearCSV() {
    var es_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

    if (es_firefox) {
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#tablita').html()));
    } else {
        var tablehtml = document.getElementById('tablita').innerHTML;
        var datos = tablehtml
            .replace(/<thead class="thead-light align-middle">/g, '')
            .replace(/<\/thead>/g, '')
            .replace(/<tbody>/g, '')
            .replace(/<\/tbody>/g, '')
            .replace(/<tr>/g, '')
            .replace(/<\/tr>/g, '\r\n')
            .replace(/<th scope="col">/g, '')
            .replace(/<th scope="col" colspan="5">/g, '')
            .replace(/<th scope="col" colspan="6">/g, '')
            .replace(/<\/th>/g, ';')
            .replace(/<td>/g, '')
            .replace(/<td colspan="5">/g, '')
            .replace(/<td colspan="6">/g, '')
            .replace(/<\/td>/g, ';')
            .replace(/\t/g, '')
            .replace(/\n/g, '');
        var link = document.createElement('a');
        link.download = 'archivo.csv';
        link.href = 'data:application/csv,' + escape(datos);
        link.click();
    }
}